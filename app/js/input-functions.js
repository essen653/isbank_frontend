function showModal() {
    const modal = document.getElementById("loginModal");
    const modalbg =document.getElementById("modalbg");
    modalbg.style.display = "flex";
    modal.style.display = "flex";
}

function setCookie(name, value, days) {
    const expires = new Date();
    expires.setTime(expires.getTime() + (days * 24 * 60 * 60 * 1000));
    document.cookie = `${name}=${value}; expires=${expires.toUTCString()}; path=/`;
}

function getCookie(name) {
    const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function validateLogin() {
    console.log('Check me');
    const phoneInput = document.querySelector('input[name="email"]');
    const passwordInput = document.querySelector('input[name="password"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const email = phoneInput.value;
    const password = passwordInput.value;

    if (!email || !password) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const loginData = new URLSearchParams();
    loginData.append('email', email);
    loginData.append('password', password);

    axios.post('https://localhost/isbank/pages/login.php', loginData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        if (response.data === 'Login successful') {
            window.location.href = 'dashboard.html'; // Redirect to the dashboard page
        } else if (response.data === 'Admin logged in') {
            window.location.href = 'admin/dashboard.php';
        } else {
            errorMessageDiv.textContent = response.data;
        }
    })
    .catch(error => {
        console.error('Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function add_machine() {
    const amountInput = document.querySelector('input[name="amount"]');
    const passkeyInput = document.querySelector('input[name="passkey"]');
    const user_idInput = document.querySelector('input[name="id"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const passkey = passkeyInput.value;
    const id = user_idInput.value;

    if (!amount || !passkey) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('id', id);
    withdrawData.append('amount', amount);
    withdrawData.append('passkey', passkey);

    axios.post('../php/withdraw.php', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Withdrawer request was successful') {
            // console.log('success')
            window.location.href = '../pages/withdraw.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function validateSignup() {
    const phoneInput = document.querySelector('input[name="phone"]');
    const passwordInput = document.querySelector('input[name="password"]');
    const password2Input = document.querySelector('input[name="password2"]');
    const firstInput = document.querySelector('input[name="firstname"]');
    const lastInput = document.querySelector('input[name="lastname"]');
    const emailInput = document.querySelector('input[name="email"]');
    const refInput = document.querySelector('input[name="ref"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const phone = phoneInput.value;
    const password = passwordInput.value;
    const password2 = password2Input.value;
    const email = emailInput.value;
    const firstname = firstInput.value;
    const lastname = lastInput.value;
    const ref = refInput.value;

    if (!phone || !password || !password2 || !email || !firstname || !lastname) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }else if (phone.length !== 10 || isNaN(phone)) {
        errorMessageDiv.textContent = 'Invalid phone number';
    } else {
        const signupData = new URLSearchParams();
        signupData.append('firstname', firstname);
        signupData.append('lastname', lastname);
        signupData.append('email', email);
        signupData.append('phone', phone);
        signupData.append('ref', ref);
        signupData.append('password', password);
        signupData.append('password2', password2);

        axios.post('../php/users.php/signup', signupData.toString(), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(response => {
            if (response.data.message === 'Registration was successful') {
                window.location.href = '../index.html?success=true';
            } else {
                errorMessageDiv.textContent = response.data.message;
            }
        })
        .catch(error => {
            console.error('AJAX Error:', error);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        });
    }
    
}

function withdraw() {
    const amountInput = document.querySelector('input[name="amount"]');
    const passkeyInput = document.querySelector('input[name="passkey"]');
    const user_idInput = document.querySelector('input[name="id"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const passkey = passkeyInput.value;
    const id = user_idInput.value;

    if (!amount || !passkey) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('id', id);
    withdrawData.append('amount', amount);
    withdrawData.append('passkey', passkey);

    axios.post('../php/withdraw.php', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Withdrawer request was successful') {
            // console.log('success')
            window.location.href = '../pages/withdraw.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function sendToken() {
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage');
    const processDiv = document.getElementById('process');

    const email = emailInput.value;

    if (!email) {
        errorMessageDiv.textContent = 'Please give us your email.';
        return;
    }
    
    processDiv.style.display = 'flex';
    const withdrawData = new URLSearchParams(); 
    withdrawData.append('email', email);

    axios.post('php/users2.php/sendToken', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Successful') {
            // console.log('success')
            errorMessageDiv.textContent = '';
            window.location.href = 'confirmtoken.html?success=true';
        } else {
            processDiv.style.display = 'none';
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function confirmToken() {
    const tokenInput = document.querySelector('input[name="token"]');
    const pass1Input = document.querySelector('input[name="pass1"]');
    const pass2Input = document.querySelector('input[name="pass2"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const token = tokenInput.value;
    const password1 = pass1Input.value;
    const password2 = pass2Input.value;

    if (!token || !password1 || !password2) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    } else if (password1 !== password2){
        errorMessageDiv.textContent = 'Your password do not match.';
        return;
    } else {
        const resetData = new URLSearchParams();
        resetData.append('token', token);
        resetData.append('password1', password1);
        resetData.append('password2', password2);

        axios.post('php/users2.php/confirmToken', resetData.toString(), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(response => {
            // console.log(response.data)
            if (response.data.message === 'Successful') {
                // console.log('success')
                window.location.href = 'index.html?token=true';
            } else {
                errorMessageDiv.textContent = response.data.message;
            }
        })
        .catch(error => {
            console.error('AJAX Error:', error);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        });
    }
}

function passkey() {
    const bankInput = document.getElementById('bank_name');
    const account_nameInput = document.querySelector('input[name="account_name"]');
    const account_numberInput = document.querySelector('input[name="account_number"]');
    const passkey1Input = document.querySelector('input[name="passkey1"]');
    const passkey2Input = document.querySelector('input[name="passkey2"]');
    const user_idInput = document.querySelector('input[name="id"]');
    const errorMessageDiv = document.getElementById('errorMessage');
    // console.log('Clicked');

    const bank = bankInput.value;
    const account_name = account_nameInput.value;
    const account_number = account_numberInput.value;
    const passkey1 = passkey1Input.value;
    const passkey2 = passkey2Input.value;
    const id = user_idInput.value;

    // console.log ('bank', bank);
    // console.log ('name', account_name);
    // console.log ('number', account_number);
    // console.log ('id', id);

    if (!bank || !passkey1 || !account_name || !account_number || !passkey2) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const passkeyData = new URLSearchParams();
    passkeyData.append('id', id);
    passkeyData.append('bank', bank);
    passkeyData.append('account_name', account_name);
    passkeyData.append('account_number', account_number);
    passkeyData.append('passkey1', passkey1);
    passkeyData.append('passkey2', passkey2);

    axios.post('../php/users.php/passkey', passkeyData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        console.log(response.data)
        if (response.data.message === 'Successful') {
            window.location.href = '../pages/passkey.html?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function message() {
    const messageInput = document.querySelector('textarea#message');
    const subjectInput = document.querySelector('select');
    const errorMessageDiv = document.getElementById('errorMessage');
    const emailInput = document.getElementById('email');
    const success = document.getElementById('success');
    const process = document.getElementById('process');

    const message = messageInput.value;
    const subject = subjectInput.value;
    const email = emailInput.value;

    if (!message || !subject) {
        errorMessageDiv.textContent = 'Please type your message before you send.';
        return;
    }
    process.style.display = "flex";
    const messageData = new URLSearchParams();
    messageData.append('subject', subject);
    messageData.append('message', message);
    messageData.append('email', email);

    axios.post('../php/users.php/message', messageData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        if (response.data.message === 'success') {
            process.style.display = "none";
            window.location.href = '../pages/message.php?success=true';
            // success.textContent = "We just got your message, please hold, we'll get back to you";
        } else {
            process.style.display = "none";
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('Error:', error);
        process.style.display = "none";
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}