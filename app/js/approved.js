
// Get references to the elements
const toggleSidebarButton = document.getElementById("toggle-sidebar");
const sidebarBox = document.getElementById("sidebar-box");

// Add a click event listener to the toggle button
toggleSidebarButton.addEventListener("click", function() {
  if (sidebarBox.style.display === "none") {
    sidebarBox.style.display = "block";
  } else {
    sidebarBox.style.display = "none";
  }
});

function changeRowCount(value) {
    var tableRows = document.querySelectorAll('#approved tbody tr');

    tableRows.forEach(function(row, index) {
        if (value === 'all' || index < value) {
        row.style.display = '';
        } else {
        row.style.display = 'none';
        }
    });
}


var defaultCount = 10;
var rowSelect = document.getElementById('showRows2');
rowSelect.value = defaultCount;
changeRowCount(defaultCount);

rowSelect.addEventListener('change', function() {
var selectedValue = rowSelect.value;
changeRowCount(selectedValue);
});

