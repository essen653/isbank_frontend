fetch("../../assets/php/noteAppAPI.php")

// fetch("../js/a.json")
.then(function(response){
    return response.json();
})
.then(function(notes){
    let placeholder = document.querySelector("#data-output");
    let out = "";
    for(let note of notes) {
        out += `
            <tr>
            <td>${note.id}</td>
            <td>${note.note}</td>
            <td><a class="btn btn-info btn-icon-text" class="mdi mdi-file-check btn-icon-append" href="../../assets/php/noteupdate.php?id=${note.id}">Edit </a>&nbsp; <a class="btn btn-danger btn-icon-text" class="mdi mdi-file-check btn-icon-append" href="../../assets/php/notedelete.php?id=${note.id}">Delete </a> </td>
            </tr>
        `;
    }

    placeholder.innerHTML = out;
})
