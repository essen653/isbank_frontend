$(document).ready(function() {
    var rowsPerP = 10;
    var currentPage = 1;  
    function showRows(rowsPerP, currentPage) {
      var rows = $("#myTable tbody tr");
      var startIndex = (currentPage - 1) * rowsPerP;
      var endIndex = startIndex + rowsPerP;
      rows.hide();
      rows.slice(startIndex, endIndex).show();
    }

    function updateDashboard(data) { 
        const phoneElement = document.getElementById('phone');
        if (phoneElement) {
            phoneElement.value = data.phone;
            phoneElement.textContent = data.phone;
        }
        const idElement = document.getElementById('id');
        if (idElement) {
            idElement.value = data.id;
            idElement.textContent = data.id;
        }
        const firstnameElement = document.getElementById('firstname');
        if (firstnameElement) {
            firstnameElement.textContent = data.firstname;
            firstnameElement.value = data.firstname;
        }
        const usersElement = document.getElementById('users');
        if (usersElement) {
            usersElement.textContent = data.total_users; 
        }
        const emailElement = document.getElementById('email');
        if (emailElement) {
            emailElement.textContent = data.email;
            emailElement.value = data.email;
        }
        const email2Element = document.getElementById('fullname');
        if (email2Element) {
            email2Element.textContent = data.fullname;
            email2Element.value = data.fullname;
        }
    }

    function initializePagination(rows) {
        var numPages = Math.ceil(rows.length / rowsPerP);
        var pagination = $(".pagination");
        function updatePagination() {
            pagination.empty();
            var prevButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&laquo;</span></a></li>");
            pagination.append(prevButton);

            var prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");

            for (var i = 1; i <= numPages; i++) {
                if (i === currentPage) {
                    var pageLink = $("<li class='page-item active'><span class='page-link'>" + i + "</span></li>");
                } else if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
                } else {
                    if (!prevEllipsis.hasClass('disabled')) {
                        pagination.append(prevEllipsis);
                        prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                    }
                    continue;
                }

                if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    pagination.append(pageLink);
                } else {
                    prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                }
            }
            var nextButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>");
            pagination.append(nextButton);

            showRows(rowsPerP, currentPage);
        }

        updatePagination();

        pagination.on("click", ".page-link", function (e) {
            e.preventDefault();
            var clickedPage = $(this).text();

            if (clickedPage === "«") {
                currentPage = Math.max(currentPage - 1, 1);
            } else if (clickedPage === "»") {
                currentPage = Math.min(currentPage + 1, numPages);
            } else {
                currentPage = parseInt(clickedPage);
            }
            updatePagination();
        });
    }
  
    function fetchUserDetailsAndusers() {
      fetch('php/session_admin.php')
        .then(response => response.json())
        .then(data => {
          updateDashboard(data);
          fetchuserDetails(data.id, initializePagination);
        })
        .catch(error => console.error('Error:', error))
        .finally(() => {
          setTimeout(fetchUserDetailsAndusers, 90000);
        });
    }

    function search() {
        var input, filter, tableBody, tr, td, i, j, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        tableBody = document.getElementById("myTable").getElementsByTagName("tbody")[0];
        tr = tableBody.getElementsByTagName("tr");
    
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td");
            var matched = false; // Flag to indicate if any column matches the search query
    
            for (j = 0; j < td.length; j++) {
                var column = td[j];
                if (column) {
                    txtValue = column.textContent || column.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        matched = true;
                        break; // Break the loop if a match is found in any column
                    }
                }
            }
    
            if (matched) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
  
    function fetchuserDetails(callback) {
      fetch(`php/admin.php/fetchusers`)
        .then(response => response.json())
        .then(users => {
          const tableBody = document.querySelector('#myTable tbody');
          if (users.length > 0) {
            if (tableBody) {
                tableBody.innerHTML = ''; 

                users.reverse();

                users.forEach(useR => {
                    const row = document.createElement('tr');

                    const idCell = document.createElement('td');
                    const id = document.createElement('span');
                    id.textContent = useR.id;
                    idCell.appendChild(id);

                    const userIdCell = document.createElement('td');
                    const userId = document.createElement('span');
                    userId.textContent = useR.fullname;
                    userIdCell.appendChild(userId);

                    const amountCell = document.createElement('td');
                    const amountText = document.createElement('strong');
                    amountText.textContent = useR.email;
                    amountCell.appendChild(amountText);

                    const desCell = document.createElement('td');
                    const des = document.createElement('span');
                    des.textContent = useR.phone;
                    desCell.appendChild(des);

                    const machineCell = document.createElement('td');
                    const machine = document.createElement('span');
                    machine.textContent = useR.fees;
                    machineCell.appendChild(machine);

                    const tokenCell = document.createElement('td');
                    const tokenText = document.createElement('strong');
                    tokenText.textContent = useR.token;
                    tokenCell.appendChild(tokenText);

                    const dateCell = document.createElement('td');
                    const dateText = document.createElement('strong');
                    dateText.textContent = useR.balance;
                    dateCell.appendChild(dateText);

                    const actionCell = document.createElement('td');
                    const actionDiv = document.createElement('div');
                    actionDiv.className = 'btn-group-vertical';
                    actionDiv.setAttribute('role', 'group');
                    actionDiv.setAttribute('aria-label', 'Basic example');
                    
                    const btnGroup = document.createElement('div');
                    btnGroup.className = 'btn-group';
                    
                    const uID = useR.id;
                    const actionButton = document.createElement('button');
                    actionButton.type = 'button';
                    actionButton.className = 'btn btn-danger';
                    actionButton.textContent = 'Delete';

                    actionButton.addEventListener('click', function() {
                        window.location.href = `php/admin.php/deleteuser?id=${uID}`; 
                    });

                    btnGroup.appendChild(actionButton);
                    actionDiv.appendChild(btnGroup);
                    actionCell.appendChild(actionDiv);

                    row.appendChild(idCell);
                    row.appendChild(userIdCell);
                    row.appendChild(amountCell);
                    row.appendChild(desCell);
                    row.appendChild(machineCell);
                    row.appendChild(tokenCell);
                    row.appendChild(dateCell);
                    row.appendChild(actionCell);

                    tableBody.appendChild(row);
                });
                initializeSearch();
                initializeDropdowns();
            }
            } else {
                tableBody.innerHTML = '<tr><td colspan="2">No users available.</td></tr>';
            }
  
          if (callback) {
            callback(tableBody.children);
          }
        })
        .catch(error => console.error('Error:', error));
    }

    function initializeDropdowns() {
        // Initialize Bootstrap dropdowns for dynamically created elements
        const dropdownToggleElements = document.querySelectorAll('.dropdown-toggle');
        dropdownToggleElements.forEach(dropdownToggle => {
            new bootstrap.Dropdown(dropdownToggle);
        });
    }

    function initializeSearch() {
        const searchInput = document.getElementById("searchInput");
        searchInput.addEventListener("input", search);
    }
    


  
    fetchUserDetailsAndusers();
  });
  