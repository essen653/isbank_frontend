function fetchUserID() {
    return fetch('../php/session_info.php')
        .then(response => response.json())
        .then(data => {
            // console.log(data.id)
            return data.id; 
        })
        .catch(error => {
            console.error('Error:', error);
            return null;
        });
}

async function fetchMachineDetails() {
    try {
        const response = await fetch('../php/users.php/myAssets');

        if (!response.ok) {
            throw new Error(`Fetch error: ${response.status} - ${response.statusText}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching machine details:', error);
        return [];
    } finally {
        setTimeout(fetchMachineDetails, 5000);
    }  
}

async function populateMachineDetails() {
    // console.log(fetchUserID());
    const machineDetails = await fetchMachineDetails();
    const machineContainer = document.getElementById('machineContainer');

    if (machineDetails.length > 0) {
        machineDetails.forEach(machine => {
            const boxCard = document.createElement('div');
            boxCard.className = 'box-card box1';
    
            const storeInnerFlex = document.createElement('div');
            storeInnerFlex.className = 'store-innerflex';
    
            const flexColumn1 = document.createElement('div');
            flexColumn1.className = 'flex-column';
            flexColumn1.style.padding = '20px';
    
            const img = document.createElement('img');
            img.className = 'box-img';
            img.src = `../images/dashboard/${machine.img}`;
            img.alt = 'plus';
    
            const button = document.createElement('button');
            button.style.border = '0px';
            button.className = 'machine-button bg-danger';
            button.innerText = 'Sell Asset';
            button.setAttribute('data-machine-id', machine.id);
    
            //Check
            button.addEventListener('click', () => {
                const modal = document.getElementById('confirmationModal');
                modal.style.display = 'flex';
    
                const confirmButton = document.getElementById('confirmButton');
                const cancelButton = document.getElementById('cancelButton');
    
                confirmButton.addEventListener('click', async () => {
                    modal.style.display = 'none';
    
                    const machineId = machine.id;
                    const userId = await fetchUserID();
    
                    const url = `../php/users.php/sell?id=${userId}&machine_id=${machineId}`;
    
                    try {
                        const response = await fetch(url);
                        const data = await response.json();
    
                        if (data && data.message) {
                            const errorMessageElement = document.getElementById('error');
                            errorMessageElement.textContent = data.message;
                            errorMessageElement.style.display = "flex";
    
                            if (data.message === 'Successful') {
                                const successElement = document.getElementById('success');
                                successElement.textContent = "Oops, you just sold your asset.";
                                successElement.style.display = "flex";
                                errorMessageElement.style.display = "none";
                            }
                        }
                    } catch (error) {
                        console.error('Error:', error);
                    }
                });
    
                cancelButton.addEventListener('click', () => {
                    modal.style.display = 'none';
                });
            });
    
            flexColumn1.appendChild(img);
            flexColumn1.appendChild(button);
    
            const flexColumn2 = document.createElement('div');
            flexColumn2.className = 'flex-column';
    
            const machineName = document.createElement('span');
            machineName.className = 'machine-name';
            machineName.innerText = machine.machine_name;
    
            const price = document.createElement('div');
            price.style.color = 'red';
            price.innerHTML = `<strong>₦${machine.price}</strong><i class="mdi mdi-arrow-down-bold small"></i>`;
    
            const revenue = document.createElement('span');
            revenue.style.fontSize = '13px';
            revenue.innerHTML = `<span>Daily profit:</span><strong>₦${machine.profit}</strong>`;
    
            const gif = document.createElement('img');
            gif.className = 'mining-gif';
            gif.src = `../images/dashboard/texture.gif`;
            gif.alt = 'myAsset';
    
            flexColumn2.appendChild(machineName);
            flexColumn2.appendChild(price);
            flexColumn2.appendChild(revenue);
            flexColumn2.appendChild(gif);
    
    
            storeInnerFlex.appendChild(flexColumn1);
            storeInnerFlex.appendChild(flexColumn2);
    
            boxCard.appendChild(storeInnerFlex);
            machineContainer.appendChild(boxCard);
        });
    } else {
        const noDataMessage = document.createElement('div');
        noDataMessage.textContent = "Sorry, you dont have any mining machines yet. Click below to buy";
        noDataMessage.style.textAlign = "center";
        noDataMessage.style.margin = "20px";

        const button2 = document.createElement('button');
        button2.style.border = '0px';
        button2.className = 'machine-button app_color';
        button2.innerText = 'Buy Machine';

        button2.addEventListener('click', function() {
            window.location.href = '../pages/store.php';
        });

        const spacingElements = [document.createElement('div'), document.createElement('br')];
        spacingElements.forEach(element => element.classList.add('spacing'));
        machineContainer.append(...spacingElements);

        const space1 = document.createElement('br');
        const space2 = document.createElement('br');
        const space3 = document.createElement('br');
        const space4 = document.createElement('br');
        const space5 = document.createElement('br');
        const space6 = document.createElement('br');
        const space7 = document.createElement('br');
        const space8 = document.createElement('br');

        machineContainer.appendChild(noDataMessage);
        machineContainer.appendChild(button2);
        machineContainer.appendChild(space1);
        machineContainer.appendChild(space2);
        machineContainer.appendChild(space3);
        machineContainer.appendChild(space4);
        machineContainer.appendChild(space5);
        machineContainer.appendChild(space6);
        machineContainer.appendChild(space7);
        machineContainer.appendChild(space8);
        return;
    }
}

window.onload = populateMachineDetails;