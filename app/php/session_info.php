<?php
session_start();

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

if (isset($_SESSION['email'])) {
    include 'config.php';
    $id = $_SESSION['email'];
    $sql = "SELECT * FROM `users` WHERE email = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $balance = $row['balance'];
        $firstname = $row['firstname'];
        $fullname = $row['fullname'];
        $id = $row['id'];
        $phone = $row['phone'];
        $email = $row['email'];
        $transaction = $row['transaction'];
        $account_number = $row['account_number'];
        $fees = $row['fees'];
        $total_deposit = $row['total_deposit'];
        $userDetails = array(
            'user_id' => $id,
            'firstname' => $firstname,
            'fullname' => $fullname,
            'phone' => $phone,
            'email' => $email,
            'id' => $id,
            'balance' => $balance,
            'account_number' => $account_number,
            'fees' => $fees,
            'transaction' => $transaction,
            'total_deposit' => $total_deposit
        );

        header('Content-Type: application/json');
        echo json_encode($userDetails);
    } else {
        echo json_encode(array('error' => 'user id not found'));
    }
} else {
    echo json_encode(array('error' => 'Not authorized'));
}
?>
