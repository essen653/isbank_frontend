const checkboxes = document.getElementsByClassName('row-checkbox');
const sendButton = document.getElementById('sendBulk');
const selectAll = document.getElementById('select-all');


Array.from(checkboxes).forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
    // Check if any checkbox is checked
    const isChecked = Array.from(checkboxes).some(function (checkbox) {
        return checkbox.checked;
    });

    

    if (isChecked) {
        sendButton.style.display = 'flex';
    } else {
        sendButton.style.display = 'none';
    }
    });
});
selectAll.addEventListener('change', function () {
    // Get the checked state of the "select-all" checkbox
    const isChecked = selectAll.checked;

    // Check or uncheck all the checkboxes based on the state of "select-all" checkbox
    Array.from(checkboxes).forEach(function (checkbox) {
    checkbox.checked = isChecked;
    });

    // Show or hide the "Send Bulk Application" button based on the checked state
    if (isChecked) {
        sendButton.style.display = 'flex';
    } else {
        sendButton.style.display = 'none';
    }
});