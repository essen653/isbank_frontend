<?php
session_name("Isbank");
session_start();

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

if (isset($_SESSION['fullname']) && isset($_SESSION['id'])) {
    include 'config.php';
    $id = $_SESSION['id'];
    $sql = "SELECT * FROM `users` WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $account_balance = $row['balance'];
        $firstname = $row['firstname'];
        $fullname = $row['fullname'];
        $user_id = $row['id'];
        $phone = $row['phone'];
        $email = $row['email'];
        $account_number = $row['account_number'];
        $userDetails = array(
            'user_id' => $user_id,
            'firstname' => $firstname,
            'fullname' => $fullname,
            'phone' => $phone,
            'email' => $email,
            'id' => $user_id,
            'account_number' => $account_number
        );

        header('Content-Type: application/json');
        echo json_encode($userDetails);
    } else {
        echo json_encode(array('error' => 'user id not found'));
    }
} else {
    echo json_encode(array('error' => 'Not authorized'));
}
?>
