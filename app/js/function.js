function transfer() {
    const amountInput = document.querySelector('input[name="amount"]');
    const emailInput = document.querySelector('input[name="email"]');
    const bankInput = document.querySelector('select[name="bank"]');
    const b_nameInput = document.querySelector('input[name="b_name"]');
    const a_numberInput = document.querySelector('input[name="a_number"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const email = emailInput.value;
    const bank = bankInput.value;
    const b_name = b_nameInput.value;
    const a_number = a_numberInput.value;

    if (!amount || !bank || !b_name || !a_number) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('amount', amount);
    withdrawData.append('email', email);

    axios.post('php/users.php/transfer', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
        .then(response => {
            // console.log(response.data)
            if (response.data.message === 'Successful') {
                // console.log('success')
                window.location.href = 'transfer.php?success=true';
            } else if (response.data.message === 'token') {
                document.querySelector("#confirmationModal").style.display = 'flex';
            }
            else {
                errorMessageDiv.textContent = response.data.message;
            }
        })
        .catch(error => {
            console.error('AJAX Error:', error);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        });
}


function get_CCC() {
    const amountInput = document.querySelector('input[name="amount"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;

    if (!amount) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }
    localStorage.getItem('mail', email);
    const withdrawData = new URLSearchParams();
    withdrawData.append('amount', amount);
    withdrawData.append('email', email);

    axios.post('php/users.php/getccc', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
        .then(response => {
            // console.log(response.data)
            if (response.data.message === 'Successful') {
                // console.log('success')
                window.location.href = 'get_code.php?success=true';
            }
            else {
                errorMessageDiv.textContent = response.data.message;
            }
        })
        .catch(error => {
            console.error('AJAX Error:', error);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        });
}

function subModal() {
    const codeInput = document.querySelector('input[name="code"]');
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage2');

    const code = codeInput.value;
    const email = emailInput.value;

    if (!code) {
        errorMessageDiv.textContent = 'Please enter your code.';
        return;
    }
    const withdrawData = new URLSearchParams();
    withdrawData.append('code', code);
    withdrawData.append('email', email);

    axios.post('php/users.php/resetToken', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
        .then(response => {
            if (response.data.message === 'Successful') {
                // console.log('success')
                window.location.href = 'transfer.php?success2=true';
            } else {
                errorMessageDiv.textContent = response.data.message;
            }
        })
        .catch(error => {
            console.error('AJAX Error:', error);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        });
}